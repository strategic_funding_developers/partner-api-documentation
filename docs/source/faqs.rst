Frequently Asked Questions
==========================

How do I get the Partner API Agreement?
---------------------------------------
Contact your Partner Manager. They will email you the document to sign.

How do I get my Sandbox API credentials?
----------------------------------------
After you return the Partner API Agreement we will response back with: swagger Partner API definition, link to the documentation resource, and the Sandbox API Keys.

What is the pricing for the Kapitus Partner API?
------------------------------------------------
Currently this is a free to use API.

Are there any Rate Limits for API usage?
----------------------------------------
Currently there are no enforced rate limits for this API. The Access Token does expire after 3600 seconds.

How can I get in touch with Kapitus developers for help?
--------------------------------------------------------
Ask your Partner Manager. While we can't guarantee that we will always have developer availability to jump on a call. We will definitely answer any questions in a timely manner.






