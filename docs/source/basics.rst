Basics
======

Getting Started
---------------
Before you start submitting submissions to Kapitus you must sign the Partner API Agreement. API Keys will not be issued until the the Partner API agreement has been signed.

Once the Partner API Agreement has been signed API Keys will be issued for the API Sandbox. You will be able to submit submissions to the Sandbox. Kapitus will validate the submissions being made and provide feedback.

When the Sandbox submission is validated the Partner will be issued Production API Keys and be able to submit to the production environment.


Authentication
--------------

The Kapitus Partner API uses a Access Token to authorize API requests.

In order to generate an Access Token this request must be made:

.. code-block:: none

    https://base_url/api/v1/authenticate?client_id={{partner_id}}&client_secret={{partner_secret}}

.. code-block:: none

    The client_id and client_secret and be provided in the request header as well.

This will return back a response with an Access Token

.. code-block:: json

    {
        "status": "success",
        "data": {
            "access_token": "9eb6a74a59c879358319a092a8547ba63e5f45f82a372f0ef86fddc658121af5",
            "expires_in": 3600
        }
    }

The Access Token is valid for 3600 seconds. Currently there are no rate limits.

Example:

.. code-block:: none

    https://base_url/api/v1/application?token={{access_token}}

.. code-block:: none

    The client_id and client_secret and be provided in the request header as well.

Response Body
-------------
All response bodies have the same breakdown.

.. code-block:: json

    {
        "status": "success",
        "data": {
            "data": "data
        }
    }


Response Codes
--------------
All responses will contain one of the following HTTP Responses.

200 - OK
^^^^^^^^
The request has succeeded.

201 - Created
^^^^^^^^^^^^^
The request has been fulfilled and has resulted in one or more new resources being created.

202 - Accepted
^^^^^^^^^^^^^^
The request has been accepted for processing, but the processing has not been completed. The request might or might not eventually be acted upon, as it might be disallowed when processing actually takes place.

400 - Bad Request
^^^^^^^^^^^^^^^^^
The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).

401 - Unauthorized
^^^^^^^^^^^^^^^^^^
The request has not been applied because it lacks valid authentication credentials for the target resource.

403 - Forbidden
^^^^^^^^^^^^^^^
The server understood the request but refuses to authorize it.

404 - Not Found
^^^^^^^^^^^^^^^
The origin server did not find a current representation for the target resource or is not willing to disclose that one exists.

500 - Internal Server Error
^^^^^^^^^^^^^^^^^^^^^^^^^^^
The server encountered an unexpected condition that prevented it from fulfilling the request.


