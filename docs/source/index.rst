.. Kapitus Partner API documentation master file

Kapitus Partner API Documentation
=================================
This is the documentation for the Kapitus Partner API.


Table of Contents
-----------------

.. toctree::

   basics
   endpoints
   faqs



