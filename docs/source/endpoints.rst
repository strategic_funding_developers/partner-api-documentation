Available Endpoints
===================
It is highly recommended to check into the Partner API Swagger file for specifics about the API documentation. The Partner API Swagger contains required fields, examples, etc.

Listed below are three main endpoints. The main difference between all three are the requirements to creation.

Applications
------------
The strictest submission. It requires a full application to be submitted along with supported documents to be processed.

Referrals
---------
A non-complete application. It cannot be processed in its current state but has enough identifying information to allow Kapitus to follow up try to process the submission into a full application.

Partials
--------
This is a open submission type that has no requirements. It is perfect to slowly collecting data from the customer and building a full application.

